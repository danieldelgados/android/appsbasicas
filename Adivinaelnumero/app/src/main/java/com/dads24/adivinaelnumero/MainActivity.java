package com.dads24.adivinaelnumero;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import java.util.Random;

public class MainActivity extends AppCompatActivity {
    int numAleatorio;

    public void generarNumerosAleatorios(){
        Random numeroAleatorio = new Random();

        numAleatorio = numeroAleatorio.nextInt(30)+ 1;
    }
    public void adivinar(View view){

        EditText numeroUsuario = findViewById(R.id.numberEditText);

        int numeroAdivinado = Integer.parseInt(numeroUsuario.getText().toString());
        String mensaje;
        if(numeroAdivinado > numAleatorio){
            mensaje = "Te has pasado";
        } else if(numeroAdivinado < numAleatorio){
            mensaje = "Te has quedado corto";
        } else{
            mensaje = "Adivinaste! Intentalo de nuevo! ";
            generarNumerosAleatorios();
        }

        Toast.makeText(this,mensaje, Toast.LENGTH_SHORT).show();
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        generarNumerosAleatorios();
    }
}
