package com.dads24.conversordemoneda;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    public void convertir(View view){
        Double pesoChileno = 757.25;
        EditText euroEditText = findViewById(R.id.euroEditText);
        String euroString = euroEditText.getText().toString();
        Double euro = Double.parseDouble(euroString);
        Double calculo = euro * pesoChileno;
        String calculoString = calculo.toString();

        Toast.makeText(this, "€" + euroString+ " son $" + calculoString + " pesos.", Toast.LENGTH_LONG).show();


    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
}
