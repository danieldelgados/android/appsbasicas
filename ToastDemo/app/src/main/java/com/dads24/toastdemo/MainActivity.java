package com.dads24.toastdemo;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    public void clickMe(View view){

        EditText nameEditText = findViewById(R.id.nameEditText);

        Log.i("Info", "Button pressed");

        Log.i("Name", nameEditText.getText().toString());

        Toast.makeText(this, "Hola " +nameEditText.getText().toString(), Toast.LENGTH_LONG).show();
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
}
