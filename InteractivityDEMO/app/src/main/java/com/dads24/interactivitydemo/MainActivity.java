package com.dads24.interactivitydemo;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {

    public void touchFunction(View view){

        EditText nameEditText =  findViewById(R.id.nameEditText);

        Log.i( "Info", "Me han tocado!");

        Log.i( "values", nameEditText.getText().toString());

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
}
