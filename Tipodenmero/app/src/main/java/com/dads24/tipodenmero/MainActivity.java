package com.dads24.tipodenmero;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    class Numero {

        int numero;

        public boolean esTriangular(){
            int x = 1;

            int numeroTriangular = 1;

            while (numeroTriangular < numero){
                x++;
                numeroTriangular = numeroTriangular +x;
            }

            return numeroTriangular == numero;

        }

        public boolean esCuadrado(){

            Double raizCuadrada = Math.sqrt(numero);// aqui calculo la raiz cuadrada de cualquier numero.

            return raizCuadrada == Math.floor(raizCuadrada);// aqui es donde compruebo si es una raiz cuadrada exacta
        }
    }
    public void comprobarNumero(View view){

        EditText editText = findViewById(R.id.editText);
        String mensaje;
        if(editText.getText().toString().isEmpty()){
            mensaje = "Ingresa un número";
        }else {
            Numero miNumero = new Numero();

            miNumero.numero = Integer.parseInt(editText.getText().toString());

            mensaje = editText.getText().toString();

            if (miNumero.esCuadrado() && miNumero.esTriangular()) {
                mensaje += " Es cuadrado y triangular";
            } else if (miNumero.esCuadrado()) {

                mensaje += " Es cuadrado pero no triangular.";
            } else if(miNumero.esTriangular()) {

                mensaje += " Es triangular pero no cuadrado.";
            }else {
                mensaje += " No es cuadrado ni triangular";
            }
        }
        Toast.makeText(this,mensaje, Toast.LENGTH_LONG).show();

    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
}
