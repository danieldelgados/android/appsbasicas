package com.dads24.animations;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

public class MainActivity extends AppCompatActivity {
    int cambio = 1;

    public void fade(View view){

        Log.i("Info", "La imagen ha sido clickeada");

        ImageView bartImageView = findViewById(R.id.bartImageView);

        ImageView homerImageView = findViewById(R.id.homerImageView);


        if(cambio == 1){
            bartImageView.animate().alpha(0).setDuration(2000);
            homerImageView.animate().alpha(1).setDuration(2000);
            cambio=0;
        }else {
            homerImageView.animate().alpha(0).setDuration(2000);
            bartImageView.animate().alpha(1).setDuration(2000);
            cambio=1;
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
}
